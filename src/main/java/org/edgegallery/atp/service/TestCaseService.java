/*
 * Copyright 2020-2021 Huawei Technologies Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.edgegallery.atp.service;

import java.util.List;
import org.edgegallery.atp.model.PageResult;
import org.edgegallery.atp.model.testcase.TestCase;
import org.edgegallery.atp.utils.exception.FileNotExistsException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface TestCaseService {

    /**
     * query all test cases.
     *
     * @param type manual or automatic
     * @param locale ch or zh
     * @param name test case name
     * @param testSuiteIds test suite id list the test case belong to
     * @return test case info list
     */
    List<TestCase> getAllTestCases(String type, String locale, String name, List<String> testSuiteIds);

    /**
     * query all test cases by pagination.
     *
     * @param type manual or automatic
     * @param locale ch or zh
     * @param name test case name
     * @param testSuiteIds test suite id list the test case belong to
     * @param limit limit
     * @param offset offset
     * @return test case info list
     */
    PageResult<TestCase> getAllTestCasesByPagination(String type, String locale, String name,
        List<String> testSuiteIds, int limit, int offset);

    /**
     * create test cases.
     *
     * @param file test case file
     * @param testCase test case info
     * @return test case info
     */
    TestCase createTestCase(MultipartFile file, TestCase testCase);

    /**
     * update test case.
     *
     * @param file test case file
     * @param testCase test case info
     * @return test case info
     */
    TestCase updateTestCase(MultipartFile file, TestCase testCase) throws FileNotExistsException;

    /**
     * delete test case.
     * 
     * @param id id
     * @return if delete successa
     */
    Boolean deleteTestCase(String id);

    /**
     * get one test case.
     *
     * @param id id
     * @return test case info
     */
    TestCase getTestCase(String id) throws FileNotExistsException;

    /**
     * download test case.
     *
     * @param id test case id
     * @return test case binary stream.
     */
    ResponseEntity<byte[]> downloadTestCase(String id);
}
